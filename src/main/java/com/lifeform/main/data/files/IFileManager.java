package com.lifeform.main.data.files;

import com.lifeform.main.IKi;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bryan on 7/17/2017.
 */
public interface IFileManager {

    boolean save();
    boolean delete();
}
