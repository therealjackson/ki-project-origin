package com.lifeform.main.blockchain;

public interface IBlockVerificationHelper {

    boolean verifyTransactions();

    boolean addTransactions();
}
