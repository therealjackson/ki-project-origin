package com.lifeform.main.blockchain;

public class MiningIncompatibleException extends Exception {
    public MiningIncompatibleException(String message) {
        super(message);
    }
}
